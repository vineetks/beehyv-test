package com.beehyv.demo.repository;

import com.beehyv.demo.domain.Content;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@SuppressWarnings("unused")
@Repository
public interface ContentRepository extends JpaRepository<Content,Long> {
}
