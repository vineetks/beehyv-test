package com.beehyv.demo;

import com.beehyv.demo.domain.Content;
import com.beehyv.demo.repository.ContentRepository;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class Scheduler {

    private final ContentRepository contentRepository;

    Scheduler(ContentRepository contentRepository) {
        this.contentRepository = contentRepository;
    }

    @Scheduled(initialDelay = 0, fixedRate = 20000)
    public void initializeContent() {
        if (contentRepository.findAll().size() >= 5) {
            return;
        }
        Content content = new Content();
        content.setContent("Random Content #" + (int)(Math.random()*100000));
        content.setCreationTime(new Date().getTime());
        contentRepository.save(content);
    }
}
