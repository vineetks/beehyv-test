package com.beehyv.demo.resource;


import com.beehyv.demo.service.ContentService;
import com.beehyv.demo.domain.ContentDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ContentResource {

    private final ContentService contentService;

    ContentResource(ContentService contentService) {
        this.contentService = contentService;
    }

    @GetMapping("/content")
    public ResponseEntity<List<ContentDTO>> getContentList() {
        List<ContentDTO> contentList = contentService.findAll();
        return ResponseEntity.ok(contentList);
    }

}
