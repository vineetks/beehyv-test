package com.beehyv.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
        System.out.println("*****************************************************\n*\t\t\t\t\t\t\t\t\t\t\t\t\t*");
        System.out.println("*\tApplication started at http://localhost:8080\t*\n*\t\t\t\t\t\t\t\t\t\t\t\t\t*");
        System.out.println("*****************************************************");
    }

}

