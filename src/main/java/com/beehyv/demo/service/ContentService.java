package com.beehyv.demo.service;

import com.beehyv.demo.domain.Content;
import com.beehyv.demo.domain.ContentDTO;
import com.beehyv.demo.repository.ContentRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ContentService {

    private final ContentRepository contentRepository;

    ContentService(ContentRepository contentRepository) {
        this.contentRepository = contentRepository;
    }

    public List<ContentDTO> findAll() {
        List<ContentDTO> contentDTOS = new ArrayList<>();
        List<Content> contentList = contentRepository.findAll();
        for (Content content: contentList) {
            contentDTOS.add(content.toDTO());
        }
        return contentDTOS;
    }


}
